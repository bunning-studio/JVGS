ADD_CUSTOM_COMMAND(
  OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/jvgslua.cpp
  COMMAND
  ${SWIG_EXECUTABLE} -lua -c++ -o ${CMAKE_CURRENT_BINARY_DIR}/jvgslua.cpp ${CMAKE_CURRENT_SOURCE_DIR}/jvgslua.i
  VERBATIM
)

ADD_LIBRARY(
    bind STATIC
    ${CMAKE_CURRENT_BINARY_DIR}/jvgslua.cpp
    ScriptManager.cpp
)

ADD_DEPENDENCIES( bind swig )

TARGET_INCLUDE_DIRECTORIES(
    bind
    PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}
)

TARGET_LINK_LIBRARIES(
    bind
    ${LUA_LIBRARIES}
    math
    core
    effect
    input
    game
    video
    audio
    sketch
)
